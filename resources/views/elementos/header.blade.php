<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  @yield('header')
  <title>@yield('titulo') Reparación de Equipos</title>
   {!!Html::style('css/bootstrap.min.css')!!}
   {!!Html::style('css/style.css')!!}
   {!!Html::style('css/font-awesome.min.css')!!}
	</head>