@extends('plantillas.plantilla')
@section('content')

<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <legend class="text-center header">Entrega de Equipo</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-ol bigicon"></i></span>
                            <div class="col-md-8">
                                 <select class="form-control" placeholder="ID Cliente" id="sel1">
                                    <option>Solicitud1</option>
                                  </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                 <select class="form-control" placeholder="ID Cliente" id="sel1">
                                    <option>Cliente1</option>
                                  </select>
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-id-badge bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::text('empleado', null,['placeholder'=>'ID Empleado', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        
                        
                        
                          <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-level-down bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::date('fechaingreso', null,['placeholder'=>'Fecha de Ingreso', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                          
                        
                          <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-money bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::number('presupuesto', null,['placeholder'=>'Presupuesto Inicial', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-level-up  bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::date('fechasalida', null,['placeholder'=>'Fecha de Salida', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                      
                          <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-money bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::number('presupuestofinal', null,['placeholder'=>'Presupuesto Inicial', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-unlink bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::text('tipofalla', null,['placeholder'=>'Tipo de Falla', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-money bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::number('presupuesto', null,['placeholder'=>'Presupuesto Inicial', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-align-justify bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::textarea('descripcion', null,['placeholder'=>'Drescripción de la falla', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Aceptar</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
	</div>
@endsection