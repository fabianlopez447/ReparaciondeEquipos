
@extends('plantillas.plantilla')
@section('content')
<?php
 $cliente=\App\Cliente::find($data->idcliente);
 $empleado=\App\Empleado::find($data->idempleado);
?>
<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm ">
                    <fieldset>
                        <legend class="text-center header">Ver Consulta</legend>
            <div class="row">
			<div class="col-md-12">
				<h4>Datos de Solicitud</h4>
				<hr>
			</div>
		
			<div class="col-md-4" >
				<label>Nombre del cliente:</label> 
                <p>{{$cliente->nombre}} {{$cliente->apellido}}</p>
			</div>
			<div class="col-md-4" >
				<label>Cédula:</label> 
				<p>{{$cliente->cedula}}</p>
			</div>

			<div class="col-md-4" >
				<label>Nombre del empleado:</label> 
				<p>{{$empleado->nombre}}</p>
			</div>
			<div class="col-md-4" >
				<label>Tipo equipo:</label> 
				<p>{{$data->tipoequipo}}</p>
			</div>
			<div class="col-md-4" >
				<label>Tipo de Falla:</label> 
				<p>{{$data->tipofalla}}</p>
			</div>
			<div class="col-md-4" >
				<label>Fecha ingreso:</label> 
				<p>{{$data->created_at}}</p>
			</div>
			<div class="col-md-12" >
				<label>Descripcion de falla:</label> 
				<p>{{$data->descripcionfalla}}</p>
			</div>
			<hr>
		</div>
         <hr>
          <h4>Entregar Equipo</h4>
          <hr>
              <div class="row">
                  <div class="col-md-4 col-md-offset-4"> 
                      {!!Form::model($data,['route'=>['consulta.update',$data->id],'method'=>'PUT', 'class'=>'form-horizontal', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
                          <div class="form-group">
                               {!!Form::number('costoadicional', null,['placeholder'=>'Agregue le costo adicional', 'class'=>'form-control'])!!}
                               {!!Form::hidden('estado', 'Terminado',['placeholder'=>'Agregue le costo adicional', 'class'=>'form-control'])!!}
                          </div>
                          <div class="form-group">
                               <button  class="btn btn-lg btn-primary pull-right " type="submit"> Entregar</button>
                          </div>
                      {!!Form::close()!!}
                  </div>
             </div>
          </div>
        </div>
    </div>
</div>
	</div>
@endsection