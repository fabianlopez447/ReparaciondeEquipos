@extends('plantillas.plantilla')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4" style="float: right;margin-bottom: 2em;">
            <div class="input-group input-group-lg">
              <input type="text" class="form-control" placeholder="ID Solicitud">
              <span class="input-group-addon">buscar</span>
            </div>
            
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <table class="table table-bordered table-hover">
                    <thead class="tablaverde">
                        <th>ID</th>
                        <th>IDCliente</th>
                        <th>Tipo de Equipo</th>
                        <th>Descripción de la falla</th>
                        <th>Fecha de Ingreso</th>
                        <th>Estado</th>
                        <th>Opción</th>
                    </thead>
                    <tbody>
                       @foreach($data1 as $datas1)
                        <tr>
                            <td>{{$datas1->id}}</td>
                            <td>{{$datas1->idcliente}}</td>
                            <td>{{$datas1->tipofalla}}</td>
                            <td>{{$datas1->descripcionfalla}}</td>
                            <td>{{$datas1->created_at}}</td>
                            <td>{{$datas1->estado}}</td>
                            <td><a href="{{route('consulta.show',$datas1->id)}}">Ver</a></td>
                        </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
 

 
 