
@extends('plantillas.plantilla')
@section('content')

<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                    <legend class="text-center header">Clientes</legend>
                   <a href="{!!route('cliente.create')!!}"><button type="button" class="btn btn-primary btn-lg btn-block">Nuevo Cliente</button></a> 
           
           <br>
             <table class="table table-bordered table-hover">
                    <thead class="tablaverde">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Cedula</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($data as $dato)
                        <tr>
                            <td>{{$dato->id}}</td>
                            <td>{{$dato->nombre}}</td>
                            <td>{{$dato->apellido}}</td>
                            <td>{{$dato->cedula}}</td>
                            <td>{{$dato->telefono}}</td>
                        </tr>      
                        @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
	</div>
@endsection

