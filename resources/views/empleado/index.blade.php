@extends('plantillas.plantilla')
@section('content')

<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                    <legend class="text-center header">Empleados</legend>
                   <a href="{!!route('empleado.create')!!}"><button type="button" class="btn btn-primary btn-lg btn-block">Agregar Empleado</button></a> 
           
           <br>
                
             <table class="table table-bordered table-hover">
                    <thead class="tablaverde">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Cedula</th>
                            <th>Telefono</th> 
                            <th>Cargo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $empleado)
                        <tr>
                                <td>{{$empleado->id}}</td>
                                <td>{{$empleado->nombre}}</td>
                                <td>{{$empleado->apellido}}</td>
                                <td>{{$empleado->cedula}}</td>
                                <td>{{$empleado->telefono}}</td>
                                <td>{{$empleado->cargo}}</td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
	</div>
	
@endsection