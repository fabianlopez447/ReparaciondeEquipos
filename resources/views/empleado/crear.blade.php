@extends('plantillas.plantilla')
@section('content')

<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    {!!Form::open(['route'=>'empleado.store','method'=>'POST', 'class'=>'form-horizontal', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
                    <fieldset>
                        <legend class="text-center header">Nuevo Empleado</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::text('nombre', null,['placeholder'=>'Nombre', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::text('apellido', null,['placeholder'=>'Apellido', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-id-card-o bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::number('cedula', null,['placeholder'=>'Cedula de identidad', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::number('telefono', null,['placeholder'=>'Telefono', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-sitemap bigicon"></i></span>
                            <div class="col-md-8">
                               <select class="form-control" name="cargo" id="sel1">
                                <option>Tecnico</option>
                                <option>Administrador</option>
                                <option>Supervisor</option>
                              </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-map-marker bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::textarea('direccion', null,['placeholder'=>'Dirección', 'class'=>'form-control'])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Aceptar</button>
                            </div>
                        </div>
                    </fieldset>
                    {!!Form::close()!!}
                </form>
            </div>
        </div>
    </div>
</div>
	</div>
@endsection