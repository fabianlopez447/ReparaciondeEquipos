@extends('plantillas.plantilla')
@section('content')

<div class="container">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">Ingresar</div>
            <div class="panel-body">
                {!!Form::open(['route'=>'auth.login.store','method'=>'POST', 'class'=>'form'])!!}
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        {!!Form::email('email', null,['placeholder'=>'Correo electrónico', 'id'=>'email', 'class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" class="form-control" name="password" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-block">Ingresar</button>
                    </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endsection