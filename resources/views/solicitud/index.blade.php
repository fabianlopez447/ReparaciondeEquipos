@extends('plantillas.plantilla')
@section('content')

<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
               <legend class="text-center header">Soliictud de Reparación</legend>
                   <a href="{!!route('cliente.create')!!}"><button type="button" class="btn btn-primary btn-lg btn-block">Nueva Solicitud</button></a> <br>s
                <table class="table table-bordered table-hover">
                    <thead class="tablaverde">
                        <tr>
                            <th>ID</th>
                            <th>Tipo de Equipo</th>
                            <th>Fecha de ingreso</th>
                            <th>Tipo de falla</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Escritorio</td>
                            <td>12/03/2016</td>
                            <td>Hardware</td>
                            <td>En proceso</td>
                            <td>Ver | Cambiar</td>
                        </tr>
                       <tr>
                            <td>2</td>
                            <td>Laptop</td>
                            <td>12/03/2016</td>
                            <td>Hardware</td>
                            <td>Terminado</td>
                            <td>Ver | Cambiar</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Escritorio</td>
                            <td>12/03/2016</td>
                            <td>Hardware</td>
                            <td>Hardware</td>
                            <td>Ver | Cambiar</td>
                        </tr>
                    </tbody>
                </table>
 
            </div>
        </div>
    </div>
</div>
	</div>
@endsection