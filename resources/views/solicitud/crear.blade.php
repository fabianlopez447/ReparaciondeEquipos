@extends('plantillas.plantilla')
@section('content')

<div class="container">
		<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                 {!!Form::open(['route'=>'solicitud.store','method'=>'POST', 'class'=>'form-horizontal', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
                    <fieldset>
                        <legend class="text-center header">Solicitud de reparación</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-laptop bigicon"></i></span>
                            <div class="col-md-8">
                                 <select class="form-control" name="tipoequipo" id="sel1">
                                    <option>Escritorio</option>
                                    <option>Portatil</option>
                                    <option>Otro</option>
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-info bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::text('informacion', null,['placeholder'=>'Información del equipo', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <select class="form-control" name="idcliente" placeholder="ID Cliente" id="sel1">
                                    @foreach($cliente as $Cliente)
                                    <option value="{{$Cliente->id}}">{{$Cliente->nombre}} {{$Cliente->apellido}} ({{$Cliente->id}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    
                       <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>
                            <div class="col-md-8">
                                <select class="form-control" name="idempleado" id="sel1">
                                    @foreach($empleado as $Empleado)
                                        <option value="{{$Empleado->id}}">{{$Empleado->nombre}} {{$Empleado->apellido}} ({{$Empleado->id}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-unlink bigicon"></i></span>
                            <div class="col-md-8">
                                 <select class="form-control" name="tipofalla" id="sel1">
                                    <option>Hardware</option>
                                    <option>Software</option>
                                    <option>Otro</option>
                                  </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-align-justify bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::textarea('descripcionfalla', null,['placeholder'=>'Drescripción de la falla', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-money bigicon"></i></span>
                            <div class="col-md-8">
                                {!!Form::number('presupuesto', null,['placeholder'=>'Presupuesto Inicial', 'class'=>'form-control'])!!}
                            </div>
                        </div>
                       <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Aceptar</button>
                            </div>
                        </div>
                    </fieldset>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
	</div>
@endsection