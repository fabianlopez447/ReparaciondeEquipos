<html>
@include('elementos.header')
<body>
<!-- Navbar -->
@include('elementos.navbar')
<!-- Fin Navbar -->	
<div class="container ui">
	@yield('content')
</div>
  
</body>
<script src="{!!asset('js/jquery.min.js')!!}"></script>
<script src="{!!asset('js/bootstrap.min.js')!!}"></script>
@yield('js')
</html>