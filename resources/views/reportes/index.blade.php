


<html>
@include('elementos.header')
<?php
 $cliente=\App\Cliente::find($data->idcliente);
 $empleado=\App\Empleado::find($data->idempleado);
?>
<body style="background: #F1F1F1;">

<div class="container ui">
	<div class="container sombra" style="background: white;margin-top: 4em;margin-bottom: 4em;padding: 2em ">
		<div class="row">
			<div class="col-md-12 " >
				<h2>Reporte</h2>
				
				<hr>
			</div>
			<div class="col-md-12">
				
			</div>
		</div>
		 <div class="row">
			<div class="col-md-12">
				<h4>Datos de Solicitud</h4>
				<hr>
			</div>
		
			<div class="col-md-4" >
				<label>Nombre del cliente:</label> 
                <p>{{$cliente->nombre}} {{$cliente->apellido}}</p>
			</div>
			<div class="col-md-4" >
				<label>Cédula:</label> 
				<p>{{$cliente->cedula}}</p>
			</div>

			<div class="col-md-4" >
				<label>Nombre del empleado:</label> 
				<p>{{$empleado->nombre}}</p>
			</div>
			<div class="col-md-4" >
				<label>Tipo equipo:</label> 
				<p>{{$data->tipoequipo}}</p>
			</div>
			<div class="col-md-4" >
				<label>Tipo de Falla:</label> 
				<p>{{$data->tipofalla}}</p>
			</div>
			<div class="col-md-4" >
				<label>Fecha ingreso:</label> 
				<p>{{$data->created_at}}</p>
			</div>
			<div class="col-md-12" >
				<label>Descripcion de falla:</label> 
				<p>{{$data->descripcionfalla}}</p>
			</div>
			<hr>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h4>Datos de Costos</h4>
				<hr>
			</div>
			<div class="col-md-12">
				<label>Costo por solicitud:</label> {{$data->presupuesto}}
			</div>
			<div class="col-md-12">
				<label>Costo adicional:</label> {{$data->costoadicional}}
			</div>
			<div class="col-md-12">
				<label>Total:</label> {{$data->costoadicional+$data->presupuesto}}
			</div>
			<div class="col-md-12">
				<button type="button" class="pull-right btn btn-default">
    <span class="glyphicon glyphicon-download"></span> Descargar
  </button>
			</div>
			
		</div>
	</div>
</div>
  
</body>
<script src="{!!asset('js/jquery.min.js')!!}"></script>
<script src="{!!asset('js/bootstrap.min.js')!!}"></script>
@yield('js')
</html>