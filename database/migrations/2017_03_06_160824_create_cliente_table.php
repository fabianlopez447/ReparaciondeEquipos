<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('cliente',function(Blueprint $table){
			$table->increments('id');
			$table->string('nombre');
            $table->string('apellido');
			$table->text('cedula');
            $table->text('telefono');  
			$table->text('direccion');  
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente');
    }
}
