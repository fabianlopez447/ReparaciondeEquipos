<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('entrega',function(Blueprint $table){
			$table->increments('id');
            $table->string('idcliente');   
			$table->string('idsolicitud');
            $table->text('idempleado');
            $table->text('fechaingreso');  
            $table->text('fechaentrega');
            $table->text('costofinal');
			$table->text('estado');  
			$table->timestamps();
		});
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('entrega');
    }
}
