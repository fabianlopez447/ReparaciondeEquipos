<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('solicitud',function(Blueprint $table){
			$table->increments('id');
            $table->string('idcliente');
            $table->string('idempleado');
			$table->string('tipoequipo');
            $table->string('tipofalla');  
            $table->text('descripcionfalla');
            $table->string('estado')->default('En proceso');
            $table->string('presupuesto');
            $table->string('costoadicional')->default('0');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitud');
    }
}
