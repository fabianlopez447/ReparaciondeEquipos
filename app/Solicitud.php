<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = 'solicitud';
    protected $fillable = ['idcliente','idempleado','tipoequipo','tipofalla','descripcionfalla','estado','presupuesto','costoadicional'];
}
