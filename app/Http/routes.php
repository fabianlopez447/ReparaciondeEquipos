<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix'=>'auth'], function(){
Route::resource('login', 'loginController');
	Route::get('logout',[
		'uses' => 'loginController@logout',
		'as'   => 'logout'
	]);
});
Route::resource('reporte','reportesController');



Route::group(['middleware'=>'auth'], function(){
	Route::get('/', function () {
	    return view('index');
	});

	Route::resource('cliente','clienteController');
	Route::post('cliente/create','clienteController@store');
	Route::resource('solicitud','solicitudController');
	Route::post('solicitud/create','solicitudController@store');
	Route::resource('empleado','empleadoController');
	Route::post('empleado/create','empleadoController@store');
	Route::resource('consulta','consultaController');
	Route::resource('entrega','entregaController');
});