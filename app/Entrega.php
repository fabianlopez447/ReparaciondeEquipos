<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrega extends Model
{
     protected $table = 'entrega';
    protected $fillable = ['idcliente','idsolicitud','idempleado','fechaingreso','fechaentrega','costofinal','estado'];
}
